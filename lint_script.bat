@echo off

REM Путь к директории проекта (где находится main.java)
set "project_dir=C:\Users\aabuinsky\IdeaProjects\laba"

REM Проверка авторского права
set "copyright=Copyright (c) Your Company"

for /r "%project_dir%" %%f in (*.java) do (
  findstr /C:"%copyright%" "%%f" > nul
  if errorlevel 1 (
    echo "Invalid copyright: %%f"
    exit /b 1
  )
)

echo "Copyright check passed."
exit /b 0
